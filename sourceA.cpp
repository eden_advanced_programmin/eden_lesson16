#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;

int callback(void* notUsed, int argc, char** argv, char** azCol);

int main()
{

	sqlite3 *db;
	int rc; // This line
	char *sql; // This line
	rc = sqlite3_open("FirstPart.db", &db);
	char *zErrMsg = 0;

	/* Create SQL statement */
	
	sql = "CREATE TABLE people("
		"id integer primary key autoincrement     NOT NULL," 
		"name          TEXT     NOT NULL);"
		"INSERT INTO people (NAME) "  \
		"VALUES ('Paul1'); " \
		"INSERT INTO people (NAME) "  \
		"VALUES ('Paul2'); " \
		"INSERT INTO people (NAME) "  \
		"VALUES ('Paul3'); " ;

	
	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		fprintf(stdout, "Table created successfully\n");
	}

	system("pause");

	sqlite3_close(db);

}


int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}