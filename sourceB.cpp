#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;

void clearTable();
const char* helper(int indicator);
int callback(void* notUsed, int argc, char** argv, char** azCol);
const char* helper2(const char* command, sqlite3* db, char* zErrMsg, int indicator);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	cout << "balanceTransfer:" << endl;
	//work
	if (balanceTransfer(4, 5, 1000, db, zErrMsg))
		cout << "complieted" << endl;
	else
		cout << "try next time" << endl;

	//not working
	if (balanceTransfer(2, 10, 15000, db, zErrMsg))
		cout << "complieted" << endl;
	else
		cout << "try next time" << endl;

	cout << "carPurchase:" << endl;;
	//working
	if (carPurchase(12, 1, db, zErrMsg))
		cout << "complieted" << endl;
	else
		cout << "try next time" << endl;

	//not working
	if (carPurchase(1, 2, db, zErrMsg))
		cout << "complieted" << endl;
	else
		cout << "try next time" << endl;

	//not working
	if (carPurchase(6, 8, db, zErrMsg))
		cout << "complieted" << endl;
	else
		cout << "try next time" << endl;

	system("pause");

	sqlite3_close(db);

}


void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

const char* helper(int indicator)
{
	auto iter = results.end();
	iter--;
	const char* s = " ";
	int size = indicator;
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i != -1 && i != -2)
			{
				s = it->second.at(i).c_str();
			}
		}
	}
	return s;
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

const char* helper2(const char* command, sqlite3* db, char* zErrMsg, int indicator)
{
	int rc;
	const char* a = " ";
	rc = sqlite3_exec(db, command, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	else
	{
		if (command[0] == 's')
			a = helper(indicator);
		else
			a = "Done";
	}
	return a;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	const char *a_car_price = "select price from cars;";
	const char *a_buyer_price = "select balance from accounts;";

	const char *a_car_available = "select available from cars;";

	clearTable();

	const char *car_price = helper2(a_car_price, db, zErrMsg, carid);
	const char *buyer_price = helper2(a_buyer_price, db, zErrMsg, buyerid);

	const char *car_available = helper2(a_car_available, db, zErrMsg, carid);

	int cp = stoi(car_price);
	int bp = stoi(buyer_price);

	int sum = bp - cp;

	if (sum >= 0 && string(car_available) == "0")
	{
		string update_sum = "UPDATE accounts set balance = " + to_string(sum) + " where ID= " + to_string(buyerid) + ";"\
			"SELECT * from accounts";

		const char* data1 = helper2(update_sum.c_str(), db, zErrMsg, buyerid);

		string update_available = "UPDATE cars set available = 1 where ID= " + to_string(carid) + ";"\
			"SELECT * from cars";

		const char* data2 = helper2(update_available.c_str(), db, zErrMsg, carid);

		if (data1 == "Done" && data2 == "Done")
			return true;

		else
		{
			cout << "There is a problem";
			return false;
		}
	}
	else
		return false;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	const char *buyer_price_from = helper2("select balance from accounts;", db, zErrMsg, from);
	int b_from = stoi(buyer_price_from);

	const char *buyer_price_to = helper2("select balance from accounts;", db, zErrMsg, to);
	int b_to = stoi(buyer_price_to);

	if (amount <= b_from)
	{
		const char *trans1 = helper2("begin transaction;", db, zErrMsg, from);

		string update_sum_from = "UPDATE accounts set balance = " + to_string(b_from - amount) + " where ID= " + to_string(from) + ";"\
			"SELECT * from accounts";

		const char* first = helper2(update_sum_from.c_str(), db, zErrMsg, from);

		string update_sum_to = "UPDATE accounts set balance = " + to_string(b_to + amount) + " where ID= " + to_string(to) + ";"\
			"SELECT * from accounts";

		const char* second = helper2(update_sum_to.c_str(), db, zErrMsg, to);

		const char *trans2 = helper2("commit;", db, zErrMsg, from);

		if (first == "Done" && second == "Done")
			return true;

		else
		{
			cout << "There is a problem";
			return false;
		}
	}

	else
		return false;
}
